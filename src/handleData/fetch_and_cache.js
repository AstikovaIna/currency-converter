import { ratesByBase } from "../utils/constants.js";
import { allPairsUrls } from "../utils/constants.js";
import { fetchRate } from "./fetch_rate.js";

export const allUrls = allPairsUrls();

export const fetchAndCache = async () => {
  let date = new Date().toLocaleDateString();

  if (Object.keys(ratesByBase).length !== 0 && localStorage.getItem(date)) return;
  await Promise.all(allUrls.map(async (url) => {
    const fromCurrency = url.substring(url.length - 7, url.length - 4);
    const toCurrency = url.substring(url.length - 3, url.length);
    const currencyPair = `${fromCurrency}-${toCurrency}`;

    const res = await fetchRate(`${url}.json`);
    const result = res[toCurrency];

    ratesByBase[currencyPair] = result;

  }))
  return localStorage.setItem(date, JSON.stringify(ratesByBase));

};