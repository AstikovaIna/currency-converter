export const fetchRate = async (url) => {

  const res = await fetch(url)
    .then((res) => res.json());

  return res;
};
