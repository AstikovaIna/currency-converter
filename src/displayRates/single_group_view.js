export const singleGroupView = (group, selector) => {

  const resultDiv = document.querySelector(selector);
  let html = '';

  if (group.length !== 0) {
    html = group.map((rate) => {
      return `<span>${rate}</span><br/>
    `
    }).concat(`<span class="count">Count: ${group.length}</span>`).join('');
    return resultDiv.innerHTML = html;

  } else {
    html = (`<span class="count">There is no rates for this range.</span>`);
    return resultDiv.innerHTML = html;
  }
}