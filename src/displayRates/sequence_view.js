export const sequenceLengthView = (result) => {

  const resultDiv = document.querySelector(".sequence");
  const html =
    `<div>The longest subsequence of rates is ${result}</div>`;

  return resultDiv.innerHTML = html;
}