import { groupOne, groupThree, groupTwo, ratesByBase } from "../utils/constants.js"
import { singleGroupView } from "../displayRates/single_group_view.js";
import { sortByGroup } from "./sort_by_group.js";
import { sequenceLengthView } from "../displayRates/sequence_view.js";

export const displayByCurrency = () => {
  const getBase = document.querySelector('.select_currency');
  const base = getBase.value.toLowerCase();

  sortByGroup(ratesByBase, base);

  singleGroupView(groupOne, '.group_one');
  singleGroupView(groupTwo, '.group_two');
  singleGroupView(groupThree, '.group_three');
  sequenceLengthView();

};

