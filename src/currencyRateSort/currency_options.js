import { currencies } from "../utils/constants.js";

export const currencyOptions = () => {
  const fromSelect = document.querySelector('.select_currency');

  const generateOptions = (options) => {
    return Object.entries(options)
      .map(
        ([currencyCode, currencyName]) =>
          `<option value="${currencyCode}">${currencyCode} - ${currencyName}</option>`
      )
      .join('');
  };

  const optionsHTML = generateOptions(currencies);
  return fromSelect.innerHTML = optionsHTML;
}

