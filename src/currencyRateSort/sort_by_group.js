import { groupOne, groupTwo, groupThree, groupByCurrency } from "../utils/constants.js";

export const sortByGroup = (rates, base) => {
  Object.entries(rates).forEach(([pair, rate]) => {

    if (pair.startsWith(base) && rate < 1) {
      groupOne.push(`${pair.toUpperCase()}: ${rate.toFixed(1)}`);
    } else if (pair.startsWith(base) && (rate >= 1 && rate < 1.5)) {
      groupTwo.push(`${pair.toUpperCase()}: ${rate.toFixed(1)}`);
    } else if (pair.startsWith(base) && rate >= 1.5) {
      groupThree.push(`${pair.toUpperCase()}: ${rate.toFixed(1)}`);
    }

    if (pair.startsWith(base)) {
      groupByCurrency.push(rate.toFixed(1));
    };
  });
  return (groupOne, groupTwo, groupThree, groupByCurrency);
}