import {
  groupOne,
  groupTwo,
  groupThree,
  groupByCurrency,
  diff,
  longestSubarray,
} from "./utils/constants.js";
import { longestSequence } from "./utils/longestSequence.js";
import { currencyOptions } from "./currencyRateSort/currency_options.js";
import { fetchAndCache } from "./handleData/fetch_and_cache.js"
import { displayByCurrency } from './currencyRateSort/sort_by_currency.js'
import { sequenceLengthView } from "./displayRates/sequence_view.js";


document.addEventListener('DOMContentLoaded', function () {

  currencyOptions();
  fetchAndCache()
    .then(() => displayByCurrency());

  document.querySelector('.select_currency').addEventListener('change', (e) => {
    groupOne.length = 0;
    groupTwo.length = 0;
    groupThree.length = 0;
    groupByCurrency.length = 0;
    displayByCurrency();

    const result = longestSequence(groupByCurrency, diff);
    sequenceLengthView(result);
    longestSubarray.length = 0;
  });

});