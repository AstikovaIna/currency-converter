
export const API_URL = 'https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/';

export const currenciesAbbreviation = ["usd", "aud", "bgn", "cad", "chf", "eur", "nzd"];

export const currencies = {
  USD: 'United States Dollar',
  AUD: 'Australian Dollar',
  BGN: 'Bulgarian Lev',
  CAD: 'Canadian Dollar',
  CHF: 'Swiss Franc',
  EUR: 'Euro',
  NZD: 'New Zealand Dollar',
};
export const diff = 0.5;
export const ratesByBase = {};

export const longestSubarray = [];

export const groupOne = [];
export const groupTwo = [];
export const groupThree = [];

export const groupByCurrency = [];

export const everyCurrencyEndpoint = currenciesAbbreviation.reduce((acc, item) => {

  acc.push(`${API_URL}${item}`);
  return acc;
}, []);

export const allCurrencyRatesUrls = (base) => {
  const baseUrl = everyCurrencyEndpoint
    .find((url) => url.endsWith(base));

  const getRates = currenciesAbbreviation

    .reduce((acc, abbrev) => {

      if (!acc[abbrev] && !baseUrl.endsWith(abbrev)) {
        acc.push(baseUrl + '/' + abbrev)
      }

      return acc;
    }, []);
  return getRates;
};

export const allPairsUrls = () => {
  const all = currenciesAbbreviation.reduce((acc, abbr) => {
    acc.push((allCurrencyRatesUrls(abbr)));
    return acc;
  }, []);
  return all.flat();
};
