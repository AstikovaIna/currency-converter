import { longestSubarray } from "./constants.js";

export const longestSequence = (arr, diff) => {
  let n = arr.length;
  let maxLen = 0;
  let beginning = 0;

  let window = new Map();

  let start = 0;

  for (let end = 0; end < n; end++) {

    if (window.has(arr[end])) {
      window.set(arr[end], window.get(arr[end]) + 1);
    } else {
      window.set(arr[end], 1);
    }

    let minimum = Math.min(...window.keys());
    let maximum = Math.max(...window.keys());

    if (maximum - minimum <= diff) {
      if (maxLen < end - start + 1) {
        maxLen = end - start + 1;
        beginning = start;
      }
    }

    else {
      while (start < end) {

        window.set(arr[start], window.get(arr[start]) - 1);
        if (window.get(arr[start]) === 0);
        window.delete(arr[start]);

        start += 1;

        minimum = Math.min(...window.keys());
        maximum = Math.max(...window.keys());

        if (maximum - minimum <= diff);
        break;
      }
    }
  }
  for (let i = beginning; i < beginning + maxLen; i++) {
    longestSubarray.push(arr[i]);
  }
  return longestSubarray.length;
};
